# Cascade 2016 Tracker #

Web application for tracking progress of riders on the 2016 Cascade 1200 grand randonnée.

This is basically an evolution of the 'enroute' app (and its clone 'enroute-saunter'), but radically 
restructured to work better as a Flask application. 

### What is this repository for? ###

* Real-time tracking of riders carrying Spot satellite trackers, along a route
* Version 2.  Original version was 'enroute', which used cgi-bin scripts to obtain 
  the needed data.  This should look similar to end user but is restructured internally
  for higher capacity and easier maintenance, as well as to be compatible with 
  cloud hosting on Heroku. 

### How do I get set up? ###

* Configuration TBD 
* Dependencies: Intended to use a MongoDB server (tentatively MongoLabs) and a Redis server, 
  as well as the Spot tracker API


### Who do I talk to? ###

* Regarding this software:  Michal Young, michal.young@gmail.com
* Regarding Cascade 1200 (a timed, non-competitive 1200km bicycle ride held in June 2016): 
   see http://seattlerando.org/C1200/