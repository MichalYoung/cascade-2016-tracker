"""
Simple Flask web site 
"""

import flask
from flask import render_template
from flask import request
from flask import url_for
from flask import jsonify # For AJAX transactions

import json
import logging
import sys

# Our own modules
import spot

###
# Globals
###
app = flask.Flask(__name__)
import CONFIG

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
  return flask.render_template('cascade.html')

@app.route("/spot")
def spot_check():
  return flask.render_template('read_spot.html')

###############
# AJAX request handlers 
#   These return JSON, rather than rendering pages. 
###############
@app.route("/_route/<routename>")
def _route(routename):
  """
  Send precomputed JSON of route points from data/routes directory
  """
  app.logger.debug("Request for route {}".format(routename))
  route_file_name = routename + ".json"
  return flask.send_from_directory("data/routes", route_file_name)

@app.route("/_readspot")
def _check():
  gid = request.args.get("gid", "", type=str)
  if gid:
    #result = "Requested {}; not implemented yet".format(gid)
    result = spot.feed(gid)
  else:
    result = "No request parameter"
  ### Matches returns a list of words
  return jsonify(result=result)

#############

app.secret_key = CONFIG.COOKIE_KEY

# Set up to run from cgi-bin script, from
# gunicorn, or stand-alone.
#
if __name__ == "__main__":
    # Standalone, with a dynamically generated
    # secret key, accessible outside only if debugging is not on
    import uuid
    app.debug=CONFIG.DEBUG
    app.logger.setLevel(logging.DEBUG)
    if app.debug: 
        print("Accessible only on localhost")
        app.run(port=CONFIG.PORT)  # Accessible only on localhost
    else:
        print("Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
else:
    # Running from cgi-bin or from gunicorn WSGI server, 
    # which makes the call to app.run.  Gunicorn may invoke more than
    # one instance for concurrent service. 
    app.debug=False

