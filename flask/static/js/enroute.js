/* 

Enroute.js

     Tracking randonneurs with Spot satellite trackers. 
     
     Michal Young, 2014-2015.
     Experimental version March 2016. 

*/

console.log("Loading dependencies: leaflet, moment"); 

leaflet = require('leaflet');
moment = require('moment');
maki = require('./markers/Leaflet.MakiMarkers.js');
makitoken = "pk.eyJ1IjoibWljaGFseW91bmciLCJhIjoiY2lwanpxdWE0MDF6d3RsbWRwcjVqMzZidCJ9.ac-iW8tr1skOOh2PMa5tkQ";
L.MakiMarkers.accessToken = makitoken;

console.log("Constructor");

function Enroute(options) {
    
    if ('center' in options) {
	this.center = options.center;
    } else {
	this.center = [40.8890347,-97.154832]; // U.S.
    }
    if ('zoom' in options) {
	this.zoom = options.zoom;
    } else {
	this.zoom = 5;
    }




    var map = L.map('map', {center:  this.center, zoom: this.zoom});
    this.map = map;
    console.log("this.map defined")
    L.MakiMarkers.accessToken =  "pk.eyJ1IjoibWljaGFseW91bmciLCJhIjoiY2lwanpxdWE0MDF6d3RsbWRwcjVqMzZidCJ9.ac-iW8tr1skOOh2PMa5tkQ";
    console.log("MakiMarkers access token provided");
    L.tileLayer('http://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png',
    {
    attribution: 'Map data <a href="http://mapbox.com">Mapbox</a>', 
    maxZoom: 18,
    id: "michalyoung.kc01ifbj"
    }).addTo(this.map);

    if ('htbin' in options) {
	this.htbin = options.htbin;
    } else {
	this.htbin = "htbin/";
    }
    var htbin = this.htbin;


    this.landmark = function(lat, lon, options) {
        var icon = L.MakiMarkers.icon(
	    {icon: options.icon || "marker-stroked",
	     color: options.color || "#FFFF00", 
	     size: "s"});
        var marker = L.marker([lat, lon],
			      {
				  title: options.title || "landmark",	
				  icon:  icon, 
				  zIndexOffset: -10
			      }).addTo(map);
	
        marker.bindPopup( options.popup || "No description provided" );
	
    }; 

    this.plot_route = function(routename, rgb) {
	console.log("Requesting track for " + routename);
	console.log("With this.map as " + this.map);
	var gpx_request=new XMLHttpRequest();
	gpx_request.map = this.map;  // Because that's what 'this' will be in the handler
	gpx_request.onreadystatechange=function() {
	    if (gpx_request.readyState == 4 && gpx_request.status == 200) {
		var points = JSON.parse(gpx_request.responseText);
		var route = L.polyline(points, { color: rgb, weight: 6} );
		console.log("Attempting to add to " + this.map);
		route.addTo(this.map);
		console.log("Plotted track for " + routename); 
	    }
	};
	console.log("Requesting gpx file " + routename); 
	gpx_request.open("Get", "/_route/" + routename, true);
	gpx_request.send();
    }; 

    if ('routes' in options) {
	console.log("Plotting routes ...");
	for (var i=0; i < options.routes.length; ++i) {
	    gpx = options.routes[i].gpx;
	    color = options.routes[i].color;
	    this.plot_route(gpx, color);
	}
    }

    var markers = { };
    var traces = { };

    this.track_spots = function(riders) {
	
	var spot_request = new XMLHttpRequest();
	spot_request.map = this.map; // For use in event handlers

	spot_request.onreadystatechange=function() {
	    console.log("Spot request state change");
	    if (spot_request.readyState == 4 && spot_request.status == 200) {
		var observations = JSON.parse(spot_request.responseText);
		for (var i=0; i < observations.features.length; ++i) {
		    feature = observations.features[i];
		    if (feature.geometry.type == "Point") {
			mark_observation(feature);
		    } else if (feature.geometry.type == "LineString") {
			trace_trajectory(feature);
		    }
		}
	    }
	};

	function trace_trajectory(feature) {
            var id = feature.properties.id;
            var name = riderName(id);
            if (traces.hasOwnProperty(id)) {
                console.log("Updating trace");
                var trace = traces[id];
                trace.setLatLngs(feature.geometry.coordinates);
            } else {
                // First trace for this id
                var trace = L.polyline(
                    feature.geometry.coordinates, 
                    { weight: 4, color: "#ff0000", opacity: 0.9,
                      dashArray: "3,7"
                    }).addTo(map);
                traces[id] = trace;
            }
	}

	function mark_observation(observation) {
	    console.log("Handling observation");
            var position = observation.geometry.coordinates;
            var id = observation.properties.id;
            var name = riderName(id);
            var obs_time = moment(observation.properties.observed_time);
            var ago = obs_time.fromNow(); 
            var obs_time_str = obs_time.format("hh:mm a<br />ddd MMM D")
                + "<br />(" + ago + ")";
	    console.log("Observation is for " + name + " at " + obs_time_str);
            if (markers.hasOwnProperty(id)) {
                console.log("Updating existing marker");
                var marker = markers[id];
                marker.setLatLng(position);
                marker.bindPopup("<b>" + name + "</b><br />" + obs_time_str );
            } else {
		console.log("Creating a new marker for " + name);
                // Initial marker structure
                var color = riderColor(id);
                var bicon = L.MakiMarkers.icon({icon: "bicycle",
						color: color, size: "m"});
                var marker = L.marker(position,
				      {
					  title: name,
					  icon: bicon, 
					  riseOnHover: true, 
				      }).addTo(map);

                marker.bindPopup("<b>" + name  + "</b> <br />" + obs_time_str);
                markers[id] = marker;
            }
	}  


	function riderName(id) {
	    var name = "Unknown";
	    for (var i=0; i < riders.length; ++i) {
		if (riders[i].feed == id) {
		    name = riders[i].name;
		}
	    }
	    return name;
	}

	function riderColor(id) {
	    var color = "#f0f0f0";
	    for (var i=0; i < riders.length; ++i) {
		if (riders[i].feed == id) {
		    color = riders[i].color;
		}
	    }
	    return color;
	}



	spot_request.ontimeout = function() { alert("Spot timed out"); }

	/* === Probe the Spot logs at 2 minute intervals === */
	function formProbeURL() {
	    var url = htbin + "spot_geojson_cached.cgi";
	    var parm_marker = "?feed=";
	    for (var i=0; i < riders.length; ++i) {
		var feed = riders[i].feed;
		url = url + parm_marker + feed;
		parm_marker = "&feed=";
	    }
	    return url;
	}

	function probe() {
	    var url = formProbeURL(); 
	    spot_request.open("Get", url, true); 
	    spot_request.send();
	    console.log("Spot request sent: " + url);
	}

	probe();
	var milliseconds = 1;
	var minutes = 60 * 1000 * milliseconds;
	window.setInterval(probe, 2 * minutes);
	console.log("Spot interval timer set to 2 minutes");


    }; /* track riders */	

}

window.Enroute = Enroute;

